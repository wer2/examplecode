#pragma once

#include <memory>
#include <string>

#ifdef NDEBUG
  #define DEBUG(msg)
#else
  #define DEBUG(msg) (Log::Instance().debug(msg, __PRETTY_FUNCTION__))
#endif

#define INFO(msg) (Log::Instance().info(msg, __PRETTY_FUNCTION__))
#define ERROR(msg) (Log::Instance().error(msg, __PRETTY_FUNCTION__))

namespace Log {
  class ILogger {
    public:
      virtual void debug(const std::string& msg, const char* func = "over") = 0;
      virtual void info(const std::string& msg, const char* func = "over") = 0;
      virtual void error(const std::string& msg, const char* func = "over") = 0;

      virtual void debug(const char* msg, const char* func = "over") = 0;
      virtual void info(const char* msg, const char* func = "over") = 0;
      virtual void error(const char* msg, const char* func = "over") = 0;
  };


  class ConsoleLogger : public ILogger {
    public:

      ConsoleLogger(const std::string& tag = "");

      void debug(const std::string& msg, const char* func) override;
      void info(const std::string& msg, const char* func) override;
      void error(const std::string& msg, const char* func) override;

      void debug(const char* msg, const char* func) override;
      void info(const char* msg, const char* func) override;
      void error(const char* msg, const char* func) override;

    private:
      std::string tag;
  };

  class NoLog : public ILogger {
    public:
      void debug(const std::string& msg, const char* func) override;
      void info(const std::string& msg, const char* func) override;
      void error(const std::string& msg, const char* func) override;

      void debug(const char* msg, const char* func) override;
      void info(const char* msg, const char* func) override;
      void error(const char* msg, const char* func) override;
  };

  ILogger& Instance();
  void SetInstance(std::unique_ptr<ILogger> lgr);
}


