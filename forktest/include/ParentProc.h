#pragma once

#include <vector>

#include "Comms.h"

namespace Parent {

  class Monitor {
    public:
      Monitor(const std::vector<Worker>& workers);
      Monitor(int workerCount);

      void startWorkers();
      void stopWorkers();

      bool areWorkersRunning();
      
      bool canRunMiscThreads();
      void setRunMiscThreads(bool canRun);

      void miscThread();

      int getWorkerCount();
      std::vector<int> getExternalComms();

    private:
      std::vector<Worker> workers;

      bool runMiscThreads = false;
  };

}

