#pragma once

#include <array>
#include <string>
#include <string_view>

//message to stop child process
constexpr char STOP[] = "STOP";

constexpr int PARENT_SOCKET = 0;
constexpr int CHILD_SOCKET = 1;

int superWrite(int socket, const std::string& msg);


struct Worker {
  pid_t pid;

  int number;

  //IPC socket
  int socket;


  int externalComs = 0;
};


