#pragma once

#include "Comms.h"

namespace Child {
  
  //Does the work of the child process
  void ChildProc(Worker child);

}
