#pragma once

#pragma GCC diagnostic ignored "-Wparentheses"
#include <gtk/gtk.h>
#pragma GCC diagnostic pop

#include "ParentProc.h"

namespace Ui {

  void RunUi(Parent::Monitor *monitor, char** argv);

}

