#pragma once

#pragma GCC diagnostic ignored "-Wparentheses"
#include <gtk/gtk.h>
#pragma GCC diagnostic pop

#include "ParentProc.h"

namespace Ui {

  //creates the display for children
  GtkWidget* createChildDisplay();

  //add children to the display
  void addChildren(Parent::Monitor* monitor);

  //clear the display
  void removeChildren();

}
