#include "Logger.h"

#include <iostream>
#include <memory>

namespace Log {

  ConsoleLogger::ConsoleLogger(const std::string& tag) : tag(tag){}

  void ConsoleLogger::debug(const std::string& msg, const char* func){
    std::cout << "[DEBUG] at " << tag << " " << func << " << " << msg << std::endl;
  }
  void ConsoleLogger::info(const std::string& msg, const char* func){
    std::cout << "[INFO] at " << tag << " " << func << " << " << msg << std::endl;
  }
  void ConsoleLogger::error(const std::string& msg, const char* func){
    std::cout << "[ERROR] at " << tag << " " << func << " << " << msg << std::endl;
  }

  void ConsoleLogger::debug(const char* msg, const char* func){
    std::cout << "[DEBUG] at " << tag << " " << func << " << " << msg << std::endl;
  }
  void ConsoleLogger::info(const char* msg, const char* func){
    std::cout << "[INFO] at " << tag << " " << func << " << " << msg << std::endl;
  }
  void ConsoleLogger::error(const char* msg, const char* func){
    std::cout << "[ERROR] at " << tag << " " << func << " << " << msg << std::endl;
  }


  void NoLog::debug(const std::string&, const char*) {}
  void NoLog::info(const std::string&, const char*) {}
  void NoLog::error(const std::string&, const char*) {}

  void NoLog::debug(const char*, const char*) {}
  void NoLog::info(const char*, const char*) {}
  void NoLog::error(const char*, const char*) {}


  static std::unique_ptr<ILogger> logger = std::make_unique<NoLog>();

  ILogger& Instance(){
    return *logger;
  }


  void SetInstance(std::unique_ptr<ILogger> lgr){
    logger = std::move(lgr);
  }

}
