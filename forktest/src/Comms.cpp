#include "Comms.h"

#include <unistd.h>

int superWrite(int socket, const std::string& msg){
  return write(socket, msg.c_str(), msg.size()+1);
}

