#include "ChildProc.h"

#include <memory>
#include <string>
#include <sstream>
#include <unistd.h>

#include <thread>

#include <arpa/inet.h>
#include <cerrno>
#include <cstdlib>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "Comms.h"
#include "Logger.h"

namespace Child {

  bool running = true;
  unsigned int externalMsgReceived = 0;
  unsigned int internalMsgReceived = 0;

  //Updates the message count to the parent periodically
  void updateStatus(const Worker& child) {
    std::stringstream ss;
    ss << "Messages Received:: Internal :" << internalMsgReceived << " External:[[" << externalMsgReceived << "]]" ;
    DEBUG(ss.str());

    superWrite(child.socket, ss.str());
  }

  //listen for broadcast messages, but in a way that doesn't stop anyone else
  int SetupBroadcastReceiver(const Worker& child){
    static const int port = 1234;
    int fd;
    int trueflag = 1;
    int reuseflag = 1;
    //have the read timeout after 1 second so it doesn't stop the thread
    struct timeval read_timeout;
    read_timeout.tv_sec = 1;
    read_timeout.tv_usec = 0;

    if((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
      superWrite(child.socket, "error creating socket");
      fd = -1;
    } else if(setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &trueflag, sizeof(trueflag)) < 0) {
      superWrite(child.socket, "error setting up broadcast receiver");
      close(fd);
      fd = -1;
    } else if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuseflag, sizeof(reuseflag)) < 0) {
      superWrite(child.socket, "error setting up receiver reuse");
      close(fd);
      fd = -1;
    } else if(setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof(read_timeout)) < 0) {
      superWrite(child.socket, "error setting up receiver timeout");
      close(fd);
      fd = -1;
    } else {
      struct sockaddr_in recv_addr;
      memset(&recv_addr, 0, sizeof(recv_addr));
      recv_addr.sin_family = AF_INET;
      recv_addr.sin_port = htons(port);
      recv_addr.sin_addr.s_addr = INADDR_ANY;

      if(bind(fd, (struct sockaddr*)&recv_addr, sizeof(recv_addr)) < 0){
        superWrite(child.socket, "error binding broadcast receiver");
        close(fd);
        fd = -1;
      }
    }

    return fd;
  }


  void listenForExternal(Worker child) {
    int bcSocket = SetupBroadcastReceiver(child);
    if(bcSocket == -1) {
      ERROR("Error creating socket");
    } else {
      static char data[2048];

      sockaddr si_other;
      unsigned int slen = sizeof(sockaddr);
      bool errorFree = true;
      while(running && errorFree){
        auto bytesRead = recvfrom(bcSocket, data, sizeof(data)-1, 0, (sockaddr *) &si_other, &slen);
        if(bytesRead != -1) {
          //filter to just messages for us
          int num = atoi(data);
          if(num == child.number){
            externalMsgReceived++;
            updateStatus(child);
          } else {
            DEBUG("Discarded broadcast message not for me");
          }
        } else if(errno != EWOULDBLOCK && errno != EAGAIN) {
          //some error happened with the socket, close the thread
          errorFree = false;
        }
        //else normal return from the non-blocking call
      }
    }
    close(bcSocket);
  }

  //main running of child process
  void ChildProc(Worker child){
    static char data[2048];

    std::string input = "";

    std::ostringstream tag;
    tag << child.number;
    Log::SetInstance(std::make_unique<Log::ConsoleLogger>(tag.str()));

    DEBUG("Hello from Child");

    auto lThread = std::thread(listenForExternal, child);

    //keep running until either the socket is closed, or we get the 
    //  appropriate message
    bool errorFree = true;
    while(running && errorFree) {
      auto bytesRead = read(child.socket, data, sizeof(data));  
      if(bytesRead != -1) {
        data[bytesRead] = '\0';

        DEBUG(data);
        input = data;
        if(input == STOP) {
          running = false;
        } else {
          internalMsgReceived++;
          //periodically update parent 
          updateStatus(child);
        }     
      } else if(errno != EWOULDBLOCK && errno != EAGAIN){
        errorFree = false;
      }
    }

    DEBUG("Close child socket");
    close(child.socket);

    lThread.join();
  }
}

