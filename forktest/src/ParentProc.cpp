#include "ParentProc.h"

#include <algorithm>
#include <chrono>
#include <string>
#include <sstream>
#include <thread>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>

#include "Comms.h"
#include "ChildProc.h"
#include "Logger.h"

namespace Parent 
{

  using namespace std::chrono_literals;
  static bool RunThreads = true;

  enum ProcessType { MAIN, CHILD };

  ProcessType spawnChildren(std::vector<Worker>& workers);
  void parentPingThread(std::vector<Worker> workers); 
  void parentStatusThread(std::vector<Worker>* workers);

  Monitor::Monitor(const std::vector<Worker>& workers) : workers(workers) {}

  Monitor::Monitor(int workerCount){
    workers.resize(workerCount);
    for(int i = 0; i < workerCount; ++i){
      workers.at(i).number = i;
    }
  }

  std::vector<int> Monitor::getExternalComms(){
    std::vector<int> results;
    std::transform(workers.begin(), workers.end(), std::back_inserter(results),
        [](auto& w) {return w.externalComs; });
    return results;
  }

  bool Monitor::areWorkersRunning() { return RunThreads; }
  int Monitor::getWorkerCount() { return workers.size(); }

  void Monitor::startWorkers() {
    RunThreads = true;
    if(spawnChildren(workers) == ProcessType::MAIN) {
      auto * w = &workers;
      std::thread(parentStatusThread, w).detach();
      std::thread(parentPingThread, workers).detach();
    } else {

    }
  }

  void Monitor::stopWorkers() {
    //send each worker the stop message
    for(auto &worker : workers){
      if(worker.pid != 0){
        superWrite(worker.socket, STOP);
      }
    }

    RunThreads = false;

    //wait for the child processes to end so they don't end up
    //  as defunct processes
    INFO("Waiting for children");
    for(auto& worker : workers){
      int status = 0;
      if(worker.pid != 0){
        close(worker.socket);
        worker.socket = 0;
        waitpid(worker.pid, &status, 0);
        worker.pid = 0;
      }
    }
    INFO("Children gone");
  }

  bool Monitor::canRunMiscThreads(){return runMiscThreads;}
  void Monitor::setRunMiscThreads(bool canRun){runMiscThreads = canRun;}

  //Thread started before the fork, 
  //  note that this thread does not run in the child process
  void Monitor::miscThread(){
    std::ostringstream ss;
    ss << "Misc Work in " << getppid();	
    while(canRunMiscThreads()){
      std::this_thread::sleep_for(1s);
	  INFO(ss.str());
    }
	ss.str("");
	ss << "Misc Work Thread ended " << getppid();
	DEBUG(ss.str());
  }

  //send pings to child until stop is sent
  void parentPingThread(std::vector<Worker> workers){ 
    const std::string ping = "Ping!";
    std::ostringstream ss;
    while(RunThreads && !workers.empty()) {
      //send a message to each worker, remove worker if it closed
      auto it = workers.begin();
      while(it != workers.end()) {
        int result;
        if((result = superWrite(it->socket, ping)) < 0){
          ss.str("");
		  ss << "Child Died: " << it->number << " " << result;
		  ERROR(ss.str());
          it = workers.erase(it);
        } else {
          ++it;
        }
      }

      std::this_thread::sleep_for(1s);
    }
	ss.str("");
    ss << "Parent Ping Thread ended" << workers.size();
	DEBUG(ss.str());
  }


  //get status messages from the child
  void parentStatusThread(std::vector<Worker>* workers){
    static char buffer[2048];
    std::string input = "";

    int count = 0;
    INFO("Parent Status Thread started");
    std::ostringstream ss;

    while(RunThreads){
      for(auto & worker : *workers){
        //socket should be made non-blocking for real code, and just handle that
        if((count = read(worker.socket, buffer, sizeof(buffer))) > 0) {
          ss.str("");
          ss << "From - " << worker.number << " - " << buffer;
		  INFO(ss.str());
          buffer[sizeof(buffer) - 1] = 0; 
          char* ext = strchr(buffer, '[');
          if(ext != nullptr){
            ext+=2;
            worker.externalComs = atoi(ext);
          }
		}
      }

      //only check for status periodically as to not thrash the cpu
      std::this_thread::sleep_for(10ms);

    }
    DEBUG("Parent Status Thread ended");
  }

  //main running of parent process

  ProcessType spawnChildren(std::vector<Worker>& workers){
    std::ostringstream ss;

    //IPC socket
    int sv[2];

    for(auto & worker : workers) {
      ss.str("");
      ss << "Creating child " << worker.number;
	  INFO(ss.str());
      //create the socket pair
      if(socketpair(AF_UNIX, SOCK_DGRAM | SOCK_NONBLOCK, 0, sv) == 0) {
        //fork a child
        worker.pid = fork();
        switch(worker.pid) {
          case -1:
            //fork failed, bail
            ERROR("Forked Failed");
            close(sv[PARENT_SOCKET]);
            close(sv[CHILD_SOCKET]);
            break;
          case 0:
            //close the parent side, so if the child dies, 
            //  the parent will know when it tries to use the other side
            close(sv[PARENT_SOCKET]);
            for(auto & w : workers){
              close(w.socket);
            }
            workers.clear();
            worker.socket = sv[CHILD_SOCKET];
            Child::ChildProc(worker);
            //in the child, no need to loop anymore
            return ProcessType::CHILD;
            break;
          default:
            worker.socket = sv[PARENT_SOCKET];
            ss.str("");
            ss << "Child ID: " << worker.pid;
            INFO(ss.str());
            //close the child side, so if the parent dies, 
            //  the child will know when it tries to use the other side
            close(sv[CHILD_SOCKET]);
            break;
        }

      } else {
        ERROR("Error creating sockets");
      }
    }

    //if we reached here, we must be in the parent
    return ProcessType::MAIN;
  }
}
