#include "ui/MainWindow.h"

#include <thread>

#include "Logger.h"
#include "ParentProc.h"

#include "ui/ChildrenDisplay.h"

namespace Ui{

  //ui events
  void activate(GtkApplication *app, gpointer user_data);
  void run_children(GtkWidget *widget, gpointer data);
  void stop_children(GtkWidget *widget, gpointer data);

  //helper functions
  void EnableRun(bool enable);

  //GTK items to update during runtime
  GtkWidget* runBtn = nullptr;
  GtkWidget* stopBtn = nullptr;

  Parent::Monitor *monitor;

  void RunUi(Parent::Monitor *mtr, char** argv){
    //Allow user control of the monitor
    monitor = mtr;
    GtkApplication *app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);

    g_signal_connect(app, "activate", G_CALLBACK(activate), nullptr);

    //this will not return until the UI finishes
    g_application_run(G_APPLICATION(app), 1, argv);

    g_object_unref(app);
  }

  //UI initialization
  void activate(GtkApplication *app, gpointer){

    auto *window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Fork!!");
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    gtk_window_set_default_size(GTK_WINDOW(window), -1, 400);

    //panel layout
    auto* grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(window), grid);

    //run button
    runBtn = gtk_button_new_with_label("Run");
    g_signal_connect(runBtn, "clicked", 
        G_CALLBACK(run_children), nullptr);
    gtk_grid_attach(GTK_GRID(grid), runBtn, 0,0,1,1);

    //top button
    stopBtn = gtk_button_new_with_label("Stop");
    gtk_widget_set_sensitive(stopBtn, false);
    g_signal_connect(stopBtn, "clicked", 
        G_CALLBACK(stop_children), nullptr);
    gtk_grid_attach(GTK_GRID(grid), stopBtn, 1,0,1,1);

    //list box
    gtk_grid_attach(GTK_GRID(grid), createChildDisplay(), 0,1,2,2);

    gtk_widget_show_all(window);

  }

  //handle the run button
  void run_children(GtkWidget *, gpointer){
    DEBUG("Run Button");
    EnableRun(false);

    if(monitor != nullptr){
      //spawn worker thread in main process, 
      //  will not be present in child process
      monitor->setRunMiscThreads(true);
      std::thread([](Parent::Monitor *monitor) 
          { monitor->miscThread();}, monitor).detach();

      //put in its own thread to not hang ui
      std::thread([](Parent::Monitor *monitor) 
          { monitor->startWorkers(); }, monitor).detach();

      addChildren(monitor);
    }

  }

  //handle the stop button
  void stop_children(GtkWidget*, gpointer){
    DEBUG("Stop Button");
    EnableRun(true);

    if(monitor != nullptr){
      //put in its own thread to not hang ui
      std::thread([](Parent::Monitor *monitor) 
          { monitor->stopWorkers(); }, monitor).detach();

      //stop other worker threads
      monitor->setRunMiscThreads(false);

      removeChildren();
    }
  }

  void EnableRun(bool enable){
    if(stopBtn != nullptr && runBtn != nullptr){
      gtk_widget_set_sensitive(runBtn, enable);
      gtk_widget_set_sensitive(stopBtn, !enable);
    }
  }

}

