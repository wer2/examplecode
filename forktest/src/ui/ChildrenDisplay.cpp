#include "ui/ChildrenDisplay.h"

#include <chrono>
#include <map>
#include <thread>

#include "Logger.h"

namespace Ui {
  using namespace std::chrono_literals;

  GtkWidget* listbox = nullptr;
  std::map<int, GtkWidget*> externalComsLabels;

  int ui_update_func(gpointer data);

  gboolean draw_up_callback(GtkWidget *widget, cairo_t *cr, gpointer data)
  {
    auto *context = gtk_widget_get_style_context(widget);

    guint width = gtk_widget_get_allocated_width(widget);
    guint height = gtk_widget_get_allocated_height(widget);

    gtk_render_background(context, cr, 0, 0, width, height);

    cairo_arc(cr, width/2.0, height/2.0, 
        MIN(width, height)/2.0,
        0, 2 * G_PI);

    GdkRGBA color {0,1,0,1};

    if(data != nullptr){

    }

    gdk_cairo_set_source_rgba(cr, &color);

    cairo_fill(cr);


    return FALSE;
  }

  GtkWidget* create_row(int launcherId)
  {
    gchar *text = g_strdup_printf("Launcher %d", launcherId);
    auto *row = gtk_list_box_row_new();

    auto *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
    g_object_set(box, "margin-start", 10, "margin-end", 10, nullptr);
    gtk_container_add(GTK_CONTAINER(row), box);

    auto *drawing_area = gtk_drawing_area_new();
    gtk_widget_set_size_request(drawing_area, 20,20);
    g_signal_connect(G_OBJECT(drawing_area), "draw",
        G_CALLBACK(draw_up_callback), NULL);

    gtk_container_add(GTK_CONTAINER(box), drawing_area);

    auto *label = gtk_label_new(text);
    gtk_container_add_with_properties(GTK_CONTAINER(box), label,
        "expand", true, nullptr);

    gchar *comsText = g_strdup_printf("Ext Coms %d", 0);
    auto *externalComsLabel = gtk_label_new(comsText);
    gtk_container_add_with_properties(GTK_CONTAINER(box), externalComsLabel, 
        "expand", true, nullptr);
    externalComsLabels.insert(externalComsLabels.begin(), std::pair<int, GtkWidget*>(launcherId, externalComsLabel));

    return row;
  }

  GtkWidget* createChildDisplay()
  {
    auto *scroll = gtk_scrolled_window_new(nullptr, nullptr);
    gtk_widget_set_hexpand(scroll, true);
    gtk_widget_set_vexpand(scroll, true);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll), 
        GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
    listbox =  gtk_list_box_new();

    gtk_list_box_set_selection_mode(GTK_LIST_BOX(listbox), 
        GTK_SELECTION_SINGLE);

    gtk_container_add(GTK_CONTAINER(scroll), listbox);

    return scroll;
  }

  void addChildren(Parent::Monitor* monitor)
  {
    for(int i = 0; i < monitor->getWorkerCount(); ++i){
      auto row = create_row(i);
      gtk_list_box_insert(GTK_LIST_BOX(listbox), row, -1);
      gtk_widget_show_all(GTK_WIDGET(row));
    }

    //ui update thread
    std::thread([monitor]() 
        {
        while(monitor->areWorkersRunning()){
          //schedule an update on the UI thread
          gdk_threads_add_idle(ui_update_func, monitor);
          //sleep for a bit
          std::this_thread::sleep_for(200ms);
        }
        }).detach();


  }

  void removeChildren(){
    auto *items = gtk_container_get_children(GTK_CONTAINER(listbox));
    for(auto *itr = items; itr != nullptr; itr = itr->next){
      auto data = itr->data;
      gtk_container_remove(GTK_CONTAINER(listbox), GTK_WIDGET(data));
    }
	externalComsLabels.clear();
  }

  int ui_update_func(gpointer data){
    auto *monitor = static_cast<Parent::Monitor*>(data);
    if(monitor != nullptr && monitor->areWorkersRunning()){
      auto externalComs = monitor->getExternalComms();
      for(int i = 0; i < static_cast<int>(externalComs.size()); ++i) {
        auto label = externalComsLabels.find(i);
        if(label != externalComsLabels.end()){
          gchar *comsText = g_strdup_printf("Ext Coms %d", externalComs[i]);
          gtk_label_set_text(GTK_LABEL(label->second), comsText);
        }

      }
    } else { ERROR("No monitor");}

    return 0;
  }
}


