#include <iostream>
#include <memory>

#include "Logger.h"
#include "ParentProc.h"
#include "ui/MainWindow.h"

int main(int argc, char** argv) {

  //parse input parameters
  int childrenCount = 1;
  if(argc > 1)
  {
    childrenCount = atoi(argv[1]);
  }

  //set up the logger
  Log::SetInstance(std::make_unique<Log::ConsoleLogger>());
  DEBUG("Logging Created");

  //create a worker per requested child
  auto monitor = std::make_unique<Parent::Monitor>(childrenCount);
 
  //setup UI
  DEBUG("Running UI");
  Ui::RunUi(monitor.get(), argv); 

  //cleanup after UI closes
  INFO("Stopping Workers");
  monitor->stopWorkers();

  INFO("Parent ended.");
  return 0;
}

