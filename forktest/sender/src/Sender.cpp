#include <array>
#include <cstdlib>
#include <iostream>
#include <numeric>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <vector>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

constexpr int MESSAGES_TO_SEND = 200;

//keep track of how many messages were sent to who
std::vector<int> msgCounts(5);

int createSocket();
void error(const char* msg);
sockaddr_in getBroadcastAddress(int PORT);
void printSummary(const std::vector<int> msgCounts);
void setupSignals();
void sig_handler(int);

template <size_t size>
int createMessage(std::array<char, size>& sbuf, int messageNumber){
  
    //pick a random target, more interesting that going in order
    int target = rand() % msgCounts.size();

    //create the msg to send
    snprintf(sbuf.data(), sbuf.size(), "%d,Hello!", target);
	
    //report that we sent a message
    std::cout << "Sent Broadcast Message #" << messageNumber <<  " labeled for target " << target << "\n";
    msgCounts[target]++;

  return strlen(sbuf.data()) + 1;

}

int main(int argc, char** argv) {
  constexpr int PORT = 1234;

  //sleep time in microseconds
  const useconds_t SLEEP_TIME = 500000;
 
  //parse the max receivers out of the first argument
  if(argc >= 2) {
    msgCounts.resize(atoi(argv[1]));
  }

  setupSignals();

  //setup randomness
  srand(time(nullptr));

  //setup the socket
  int sockfd = createSocket();
  sockaddr_in send_addr = getBroadcastAddress(PORT);

  std::array<char, 256> sbuf = {};
  //send some broadcast messages
  for(int i = 0; i < MESSAGES_TO_SEND; ++i){
    int msgSize = createMessage(sbuf, i);
    //broadcast it out
    if((sendto(sockfd, sbuf.data(), msgSize, 0, (sockaddr*) &send_addr, sizeof(send_addr))) < 0) {
      error("ERROR: send");
    }
    //wait before sending the next message
    usleep(SLEEP_TIME);
  }

  printSummary(msgCounts);

  close(sockfd);

  return 0;
}

void error(const char* msg){
  std::cerr << msg << std::endl;
  exit(1);
}

int createSocket(){

  int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sockfd < 0) {
    error("ERROR Opening Socket");
  }

  int trueflag = 1;
  if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &trueflag, sizeof(trueflag)) < 0){
    error("ERROR setsockopt reuse");
  }
  if(setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &trueflag, sizeof(trueflag)) < 0){
    error("ERROR setsockopt bcast");
  }

  return sockfd;

}

sockaddr_in getBroadcastAddress(int port){
  sockaddr_in send_addr;
  memset(&send_addr, 0, sizeof(send_addr));
  send_addr.sin_family = AF_INET;
  send_addr.sin_port = htons(port);
  inet_aton("127.255.255.255", &send_addr.sin_addr);

  return send_addr;
}

void printSummary(const std::vector<int> msgCounts){
  std::cout << "\nSUMMARY\nTarget\tMessage Count";
  for(int target = 0; target < msgCounts.size(); ++target) {
	std::cout << "\n" << target << "\t" << msgCounts[target];
  }
  int totals = std::accumulate(msgCounts.begin(), msgCounts.end(), 0);
  std::cout << "\nTOTAL: " << totals  << "\n";
}

void setupSignals(){
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = sig_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, nullptr);
}

void sig_handler(int){
  printSummary(msgCounts);  
  exit(0);
}

